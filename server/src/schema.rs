// @generated automatically by Diesel CLI.

diesel::table! {
    categories (id) {
        id -> Integer,
        name -> Text,
    }
}

diesel::table! {
    categorymap (id) {
        id -> Integer,
        exercise_id -> Integer,
        category_id -> Integer,
    }
}

diesel::table! {
    exercises (id) {
        id -> Integer,
        exercisetypeid -> Integer,
        t -> Timestamp,
        notes -> Text,
        kg -> Float,
        sets -> Integer,
        reps -> Integer,
        duration -> Integer,
    }
}

diesel::table! {
    exercisetype (id) {
        id -> Integer,
        name -> Text,
        description -> Text,
        kg -> Bool,
        sets -> Bool,
        reps -> Bool,
        duration -> Bool,
    }
}

diesel::table! {
    users (id) {
        id -> Integer,
        username -> Text,
        email -> Text,
        password -> Text,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    categories,
    categorymap,
    exercises,
    exercisetype,
    users,
);

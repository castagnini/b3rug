

use diesel::{prelude::*, sqlite::SqliteConnection};
use crate::schema::{users, categories, categorymap, exercisetype, exercises};
use crate::model::{ NewUser, User, NewCategory, Category,  NewCategoryMap, CategoryMap, NewExerciseType, ExerciseType, NewExercise, Exercise};

pub fn establish_connection() -> SqliteConnection {
    let db = "./b3rugdb.sqlite3";
    SqliteConnection::establish(db)
        .unwrap_or_else(|_| panic!("Error connecting to {}", db))
}

macro_rules! create_create_functions {
        ($func_name:ident, $entrytype: ty, $tabletype:ident) => {
        pub fn $func_name(connection: &mut SqliteConnection, entry:$entrytype ) -> Vec<i32> {
        let id_vec = diesel::insert_into($tabletype::table)
        .values(entry)
        .returning($tabletype::id)
        .get_results(connection);
        match id_vec {
            Ok(v) => v,
            Err(h) => { println!("{:?}", h); vec![] }
            }
        }
    };
}
create_create_functions!(create_user, NewUser, users);
create_create_functions!(create_category, NewCategory, categories);
create_create_functions!(create_categorymap, NewCategoryMap, categorymap);
create_create_functions!(create_exercisetype, NewExerciseType, exercisetype);
create_create_functions!(create_exercise, NewExercise, exercises);

macro_rules! create_delete_functions {
    ($func_name:ident, $tabletype:ident) => {
        pub fn $func_name(connection: &mut SqliteConnection,  idx: i32) {
        diesel::delete($tabletype::table.filter($tabletype::id.eq(idx)))
        .execute(connection)
        .expect("Error deleting entry");
        }
    };
}
create_delete_functions!(delete_user, users);
create_delete_functions!(delete_category, categories);
create_delete_functions!(delete_categorymap, categorymap);
create_delete_functions!(delete_exercisetype, exercisetype);
create_delete_functions!(delete_exercise, exercises);

pub fn delete_exercise_by_exercisetypeid(connection: &mut SqliteConnection, idx:i32) {
    diesel::delete(exercises::table.filter(exercises::exercisetypeid.eq(idx)))
            .execute(connection)
            .expect("Error query by id entry");
}

pub fn delete_categorymap_by_exercisetypeid(connection: &mut SqliteConnection, idx:i32) {
    diesel::delete(categorymap::table.filter(categorymap::exercise_id.eq(idx)))
            .execute(connection)
            .expect("Error query by id entry");
}

pub fn delete_categorymap_by_categoryid(connection: &mut SqliteConnection, idx:i32) {
    diesel::delete(categorymap::table.filter(categorymap::category_id.eq(idx)))
            .execute(connection)
            .expect("Error query by id entry");
}

macro_rules! create_update_functions {
    ($func_name:ident, $entrytype: ty, $tabletype:ident) => {
        pub fn $func_name(connection: &mut SqliteConnection,  idx: i32, entry:$entrytype) {
        diesel::update($tabletype::table.filter($tabletype::id.eq(idx)))
        .set(&entry)
        .execute(connection)
        .expect("Error deleting entry");
        }
    };
}
create_update_functions!(update_user, NewUser, users);
create_update_functions!(update_category, NewCategory, categories);
create_update_functions!(update_categorymap, NewCategoryMap, categorymap);
create_update_functions!(update_exercisetype, NewExerciseType, exercisetype);
create_update_functions!(update_exercise, NewExercise, exercises);

macro_rules! create_query_functions {
    ($func_name:ident, $entrytype: ty, $tabletype:ident) => {
        pub fn $func_name(connection: &mut SqliteConnection) -> Vec<$entrytype> {
        $tabletype::table
        .load::<$entrytype>(connection)
        .expect("Error loading tasks")
        }
    };
}
create_query_functions!(query_users, User, users);
create_query_functions!(query_categories, Category, categories);
create_query_functions!(query_categorymap, CategoryMap, categorymap);
create_query_functions!(query_exercisetypes, ExerciseType, exercisetype);
create_query_functions!(query_exercises, Exercise, exercises);

macro_rules! create_query_by_id_functions {
    ($func_name:ident, $entrytype:ty, $tabletype:ident) => {
        pub fn $func_name(connection: &mut SqliteConnection,  idx: i32) -> Vec<$entrytype> {
            $tabletype::table.filter($tabletype::id.eq(idx))
            .load::<$entrytype>(connection)
            .expect("Error query by id entry")
        }
    };
}
create_query_by_id_functions!(query_users_by_id, User, users);
create_query_by_id_functions!(query_categories_by_id, Category, categories);
create_query_by_id_functions!(query_categorymap_by_id, CategoryMap, categorymap);
create_query_by_id_functions!(query_exercisetypes_by_id, ExerciseType, exercisetype);
create_query_by_id_functions!(query_exercises_by_id, Exercise, exercises);

pub fn query_categorymap_by_exercisetypeid(connection: &mut SqliteConnection, idx:i32) -> Vec<CategoryMap> {
    categorymap::table.filter(categorymap::exercise_id.eq(idx))
            .load::<CategoryMap>(connection)
            .expect("Error query by id entry")

}

pub fn accept_user(connection: &mut SqliteConnection, user: &User) -> bool {
    let v = users::table.filter(users::username.eq(&user.username))
            .filter(users::password.eq(&user.password))
            .load::<User>(connection)
            .expect("Error query by id entry");

    if v.len() > 0 {
        return true;
    } else {
        return false;
    }
}




// #[path = "../../model/model.rs"] pub mod model;
#[path = "../../model/model.rs"] pub mod model;

pub mod schema;
pub mod db;

#[macro_use] extern crate rocket;
use db::{create_user, query_users};
use db::{create_category, query_categories, query_categories_by_id, delete_category, update_category, establish_connection };
use db::{create_categorymap, query_categorymap_by_exercisetypeid, query_categorymap, query_categorymap_by_id, delete_categorymap,delete_categorymap_by_exercisetypeid, delete_categorymap_by_categoryid, update_categorymap};
use db::{create_exercisetype, query_exercisetypes, query_exercisetypes_by_id, delete_exercisetype, update_exercisetype};
use db::{create_exercise, query_exercises, query_exercises_by_id, delete_exercise, update_exercise, delete_exercise_by_exercisetypeid};
use model::{JsonInt32, JsonCategory, JsonCategoryMap, JsonExercise, JsonExerciseType, JsonUser};
use model::{ NewCategory, NewCategoryMap, NewExerciseType, NewExercise, NewUser, User};
use rocket::serde::json::Json;
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::{Request, Response};
//use openbsd::unveil;
//use openbsd::pledge;
use rocket::request;
 use rocket::request::FromRequest;
 use rocket::request::Outcome;
 use rocket::http::Status;
use crate::db::accept_user;

/// Catches all OPTION requests in order to get the CORS related Fairing triggered.
#[options("/<_..>")]
fn all_options() {
    /* Intentionally left empty */
}

#[derive(Debug)]
pub enum ApiUserError {
    Missing,
    Invalid,
}
    

#[rocket::async_trait]
impl<'r> FromRequest<'r> for User {
    type Error = ApiUserError;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let username:String;
        let email:String;
        let password:String;

        match request.headers().get_one("username") {
            Some(token) => {
                username = token.to_string();
            }
            None => {return Outcome::Error((Status::Unauthorized, ApiUserError::Missing));}
        }

        match request.headers().get_one("email") {
            Some(token) => {
                email = token.to_string();
            }
            None => { return Outcome::Error((Status::Unauthorized, ApiUserError::Missing));}
        }
        
        match request.headers().get_one("password") {
            Some(token) => {
                password = token.to_string();
            }
            None => {return Outcome::Error((Status::Unauthorized, ApiUserError::Missing));}
        }

        let mut conn = establish_connection();

        let u =  User {
            id: 0,
            username: username,
            email: email,
            password:password
        };

        if accept_user(&mut conn,& u) {
            Outcome::Success(u)
        } else {
            Outcome::Error((Status::Unauthorized, ApiUserError::Missing))
        }
    }
}

macro_rules! create_get_hook {
    ($func_name:ident, $hook_name:literal, $query_func_name:ident, $vectype:ident ) => {

#[allow(dead_code)]
#[get($hook_name)]
fn $func_name() -> Json<$vectype> {
    let mut response = { $vectype { data: vec![], } };

    let mut conn = establish_connection();
    for entry in $query_func_name (&mut conn) {
        response.data.push(entry);
    }

    Json(response)
}

    };
}

create_get_hook!(get_user_hook, "/user", query_users, JsonUser);
create_get_hook!(get_category_hook, "/category", query_categories, JsonCategory);
create_get_hook!(get_categorymap_hook, "/categorymap", query_categorymap, JsonCategoryMap);
create_get_hook!(get_exercisetype_hook, "/exercisetype", query_exercisetypes, JsonExerciseType);
create_get_hook!(get_exercise_hook, "/exercise", query_exercises, JsonExercise);

macro_rules! create_get_by_id_hook {
    ($func_name:ident, $hook_name:literal, $query_func_name:ident, $vectype:ident ) => {

#[get($hook_name)]
fn $func_name(id: i32) -> Json<$vectype> {
    let mut response = { $vectype { data: vec![], } };

    let mut conn = establish_connection();
    for entry in $query_func_name (&mut conn, id) {
        response.data.push(entry);
    }

    Json(response)
}

    };
}
create_get_by_id_hook!(get_by_id_category_hook, "/category/by_id/<id>", query_categories_by_id, JsonCategory);
create_get_by_id_hook!(get_by_id_categorymap_hook, "/categorymap/by_id/<id>", query_categorymap_by_id, JsonCategoryMap);
create_get_by_id_hook!(get_by_id_exercisetype_hook, "/exercisetype/by_id/<id>", query_exercisetypes_by_id, JsonExerciseType);
create_get_by_id_hook!(get_by_id_exercise_hook, "/exercise/by_id/<id>", query_exercises_by_id, JsonExercise);



macro_rules! create_add_hook {
    ($func_name:ident, $hook_name:literal, $create_func_name:ident, $entrytype:ty) => {

#[allow(dead_code)]
#[post($hook_name, data = "<json_new_entry>")]
fn $func_name(json_new_entry: Json<$entrytype>, _user : User) -> Json<JsonInt32> {
    let mut response = { JsonInt32 { data: vec![], } };
   
    let entry = json_new_entry.0;
    let mut conn = establish_connection();
    for entry in $create_func_name(&mut conn, entry) {
        response.data.push(entry);
    }

    Json(response)
}

    };
}
create_add_hook!(add_user_hook, "/user/add", create_user, NewUser);
create_add_hook!(add_category_hook, "/category/add", create_category, NewCategory);
create_add_hook!(add_categorymap_hook, "/categorymap/add", create_categorymap, NewCategoryMap);
create_add_hook!(add_exercisetype_hook, "/exercisetype/add", create_exercisetype, NewExerciseType);
create_add_hook!(add_exercise_hook, "/exercise/add", create_exercise, NewExercise);

#[post("/categorymap/update_ex_id/<id>", data = "<json_new_entry>")]
fn update_categorymap_hook (id:i32, json_new_entry: Json<JsonInt32>, _user : User) -> Json<bool> {

    let mut conn = establish_connection();
    let v = query_categorymap_by_exercisetypeid(&mut conn, id);

    for cm in &v {
        if ! json_new_entry.0.data.iter().any(|&i| i == cm.category_id) {
            delete_categorymap(&mut conn, cm.id);
        }
    }

    for i in &json_new_entry.data {
        if ! v.iter().any(|cm| cm.category_id == *i) {
            let entry = NewCategoryMap { exercise_id: id, category_id:*i };
            create_categorymap(&mut conn, entry);
        }
    }

    Json(true)
}

macro_rules! create_edit_hook {
    ($func_name:ident, $hook_name:literal, $update_func_name:ident, $entrytype:ty) => {

#[post($hook_name, data = "<json_new_entry>")]
fn $func_name(id: i32, json_new_entry: Json<$entrytype>, _user:User) -> Json<bool> {
    let entry = json_new_entry.0;
    let mut conn = establish_connection();
    $update_func_name(&mut conn, id, entry);

    Json(true) 
}

    };
}
create_edit_hook!(edit_category_hook, "/category/edit/<id>", update_category, NewCategory);
create_edit_hook!(edit_categorymap_hook, "/categorymap/edit/<id>", update_categorymap, NewCategoryMap);
create_edit_hook!(edit_exercisetype_hook, "/exercisetype/edit/<id>", update_exercisetype, NewExerciseType);
create_edit_hook!(edit_exercise_hook, "/exercise/edit/<id>", update_exercise, NewExercise);

macro_rules! create_delete_hook {
    ($func_name:ident, $hook_name:literal, $delete_func_name:ident) => {

#[delete($hook_name)]
fn $func_name(id: i32, _user:User) -> Json<bool> {
    let mut conn = establish_connection();
    $delete_func_name(&mut conn, id);

    Json(true) 
}

    };
}
create_delete_hook!(delete_category_hook, "/category/del/<id>", delete_category);
create_delete_hook!(delete_categorymap_by_cat_id_hook, "/categorymap/del_by_cat_id/<id>", delete_categorymap_by_categoryid);
create_delete_hook!(delete_categorymap_by_ex_id_hook, "/categorymap/del_by_ex_id/<id>", delete_categorymap_by_exercisetypeid);
create_delete_hook!(delete_categorymap_hook, "/categorymap/del/<id>", delete_categorymap);
create_delete_hook!(delete_exercisetype_hook, "/exercisetype/del/<id>", delete_exercisetype);
create_delete_hook!(delete_exercise_hook, "/exercise/del/<id>", delete_exercise);
create_delete_hook!(delete_exercise_by_type_id_hook, "/exercise/del_by_type_id/<id>", delete_exercise_by_exercisetypeid);

pub struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "Cross-Origin-Resource-Sharing Fairing",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, PATCH, PUT, DELETE, HEAD, OPTIONS, GET",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}


#[launch]
fn rocket() -> _ {
/*    
    _ = unveil("/etc/ssl/castagnini.org.fullchain.pem", "r");
    _ = unveil("/etc/ssl/private/castagnini.org.key", "r");
    _ = unveil("./Rocket.toml", "r");
    _ = unveil("/dev/urandom", "r");
    _ = unveil("./b3rugdb.sqlite3-wal", "rwc");
    _ = unveil("./b3rugdb.sqlite3-journal", "rwc");
    _ = unveil("./b3rugdb.sqlite3", "rwc");
    unveil::disable();
    _ = pledge("stdio recfd ", "");
  */  

    rocket::build().attach(Cors)
    .mount("/", routes![all_options])
    .mount("/", routes![delete_category_hook])
    .mount("/", routes![delete_categorymap_hook])
    .mount("/", routes![delete_categorymap_by_cat_id_hook])
    .mount("/", routes![delete_categorymap_by_ex_id_hook])
    .mount("/", routes![delete_exercisetype_hook])
    .mount("/", routes![delete_exercise_hook])
    .mount("/", routes![delete_exercise_by_type_id_hook])
    .mount("/", routes![get_by_id_category_hook])
    .mount("/", routes![get_by_id_categorymap_hook])
    .mount("/", routes![get_by_id_exercisetype_hook])
    .mount("/", routes![get_by_id_exercise_hook])
//    .mount("/", routes![get_user_hook])
    .mount("/", routes![get_category_hook])
    .mount("/", routes![get_categorymap_hook])
    .mount("/", routes![get_exercisetype_hook])
    .mount("/", routes![get_exercise_hook])
//    .mount("/", routes![add_user_hook])
    .mount("/", routes![add_category_hook])
    .mount("/", routes![add_categorymap_hook])
    .mount("/", routes![add_exercisetype_hook])
    .mount("/", routes![add_exercise_hook])
    .mount("/", routes![edit_category_hook])
    .mount("/", routes![update_categorymap_hook])
    .mount("/", routes![edit_categorymap_hook])
    .mount("/", routes![edit_exercisetype_hook])
    .mount("/", routes![edit_exercise_hook])
}



CREATE TABLE exercises (      
    id INTEGER NOT NULL,  
    exercisetypeid INTEGER NOT NULL,  
    t TIMESTAMP NOT NULL,
    notes TEXT NOT NULL,    
    kg FLOAT NOT NULL,    
    sets INTEGER NOT NULL,    
    reps INTEGER NOT NULL,
    duration INTEGER NOT NULL,    
    PRIMARY KEY (id)      
);                        

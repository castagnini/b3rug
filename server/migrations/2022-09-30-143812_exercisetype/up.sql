CREATE TABLE exercisetype (      
    id INTEGER NOT NULL,  
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    kg BOOLEAN NOT NULL,    
    sets BOOLEAN NOT NULL,    
    reps BOOLEAN NOT NULL,    
    duration BOOLEAN NOT NULL,    
    PRIMARY KEY (id)      
);                        


//#[path = "../schema.rs"] mod schema;
#[cfg(target_family = "unix")]
use diesel::{prelude::*};
#[cfg(target_family = "unix")]
use crate::schema::{categories, categorymap, exercisetype, exercises, users};

use serde::Serialize;
use serde::Deserialize;

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Insertable, AsChangeset))]
#[cfg_attr(target_family = "unix", diesel(table_name = users))]
pub struct NewUser<'a> {
    pub username: &'a str, 
    pub email: &'a str, 
    pub password: &'a str, 
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Queryable))]
pub struct User {
    pub id: i32,
    pub username:  String, 
    pub email:  String, 
    pub password:  String, 
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Insertable, AsChangeset))]
#[cfg_attr(target_family = "unix", diesel(table_name = categories))]
pub struct NewCategory<'a> {
    pub name: &'a str, 
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Queryable))]
pub struct Category {
    pub id: i32,
    pub name:  String, 
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Insertable,AsChangeset))]
#[cfg_attr(target_family = "unix", diesel(table_name = categorymap))]
pub struct NewCategoryMap {
    pub exercise_id: i32, 
    pub category_id: i32,
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Queryable))]
pub struct CategoryMap {
    pub id: i32,
    pub exercise_id: i32, 
    pub category_id: i32,
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Insertable,AsChangeset))]
#[cfg_attr(target_family = "unix", diesel(table_name = exercises))]
pub struct NewExercise <'a>{
    pub exercisetypeid: i32,
    pub t:   chrono::NaiveDateTime, 
    pub notes: &'a str,
    pub kg: f32,
    pub sets: i32,
    pub reps: i32,
    pub duration: i32
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Queryable))]
pub struct Exercise {
    pub id: i32,
    pub exercisetypeid: i32,
    pub t:   chrono::NaiveDateTime, 
    pub notes: String,
    pub kg: f32,
    pub sets: i32,
    pub reps: i32,
    pub duration: i32
}

#[derive(Serialize, Debug, Deserialize)]
#[cfg_attr(target_family = "unix", derive(Insertable,AsChangeset))]
#[cfg_attr(target_family = "unix", diesel(table_name = exercisetype))]
pub struct NewExerciseType<'a> {
    pub name: &'a str,
    pub description: &'a str,
    pub kg: bool,
    pub sets: bool,
    pub reps: bool,
    pub duration: bool
}

#[derive(Serialize, Debug, Deserialize, Clone)]
#[cfg_attr(target_family = "unix", derive(Queryable))]
pub struct ExerciseType {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub kg: bool,
    pub sets: bool,
    pub reps: bool,
    pub duration: bool
}

macro_rules! create_json_vec {
    ($vecname:ident, $entrytype:ty) => { 

#[derive(Serialize, Debug, Deserialize)]
pub struct $vecname {
    pub data: Vec<$entrytype>,
}

};
}
create_json_vec!(JsonUser, User);
create_json_vec!(JsonCategory, Category);
create_json_vec!(JsonCategoryMap, CategoryMap);
create_json_vec!(JsonExerciseType, ExerciseType);
create_json_vec!(JsonExercise, Exercise);
create_json_vec!(JsonInt32, i32);




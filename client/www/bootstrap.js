init();

async function init() {
    if (typeof process == "object") {
        // We run in the npm/webpack environment.
        const [{Chart, dashboard, categories, exercises, new_activity, analysis, settings }, {main, setup}] = await Promise.all([
            import("wasm-demo"),
            import("./index.js"),
        ]);
        main();
		console.log("fanculo")
    } else {
        const [{Chart, login, logout, rondella, dashboard, categories, new_category, new_exercise, remove_category, edit_category_page, modify_category, exercises, remove_exercise, add_activity, remove_activity, new_activity, analysis, settings, change_exercise_event, change_category_event, manage_user, default: init}, {main, setup}] = await Promise.all([
            import("../pkg/b3rugwasm.js"),
            import("./index.js"),
        ]);
        await init();
        //setup(Chart);
		window.login = login;
		window.logout = logout;
		window.rondella = rondella;
		window.dashboard = dashboard;
		window.categories = categories;
		window.new_category = new_category;
		window.new_exercise = new_exercise;
		window.edit_category_page = edit_category_page;
		window.modify_category = modify_category;
		window.remove_category = remove_category;
		window.remove_exercise = remove_exercise;
		window.change_exercise_event = change_exercise_event;
		window.change_category_event = change_category_event;
        window.exercises = exercises;
        window.new_activity = new_activity; 
        window.add_activity = add_activity; 
        window.remove_activity = remove_activity; 
        window.analysis = analysis;
        window.settings = settings;
        window.manage_user = manage_user;
    	 await rondella();
		
        main();
    }
}

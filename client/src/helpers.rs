use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{HtmlElement, HtmlInputElement, HtmlSelectElement, HtmlImageElement, HtmlCanvasElement, HtmlSpanElement, HtmlTableRowElement};

// Log a message to the console
#[wasm_bindgen]
extern "C" {
    fn console_log(s: &str);
    fn console_error(s: &str);
}

// Function to get a document element by ID and cast it generically, logs error before panicking
fn get_element_by_id<T: JsCast>(id: &str) -> T {
    let window = web_sys::window().expect("Failed to retrieve window");
    let document = window.document().expect("Failed to retrieve document");

    let element = document.get_element_by_id(id);
    if element.is_none() {
        console_error(&format!("Element with ID '{}' not found", id));
        panic!("Element not found");
    }

    let element = element.unwrap();

    let casted_element = element.dyn_into::<T>();
    if casted_element.is_err() {
        console_error(&format!("Failed to cast element with ID '{}' to the requested type", id));
        panic!("Failed to cast element");
    }

    casted_element.unwrap()
}

#[allow(dead_code)]
pub fn get_canvas_element_by_id(id: &str) -> HtmlCanvasElement {
    get_element_by_id::<HtmlCanvasElement>(id)
}

#[allow(dead_code)]
pub fn get_image_element_by_id(id: &str) -> HtmlImageElement {
    get_element_by_id::<HtmlImageElement>(id)
}

pub fn get_span_element_by_id(id: &str) -> HtmlSpanElement {
    get_element_by_id::<HtmlSpanElement>(id)
}

pub fn get_input_element_by_id(id: &str) -> HtmlInputElement {
    get_element_by_id::<HtmlInputElement>(id)
}

pub fn get_select_element_by_id(id: &str) -> HtmlSelectElement {
    get_element_by_id::<HtmlSelectElement>(id)
}

pub fn get_html_element_by_id(id: &str) -> HtmlElement {
    get_element_by_id::<HtmlElement>(id)
}

pub fn get_table_row_element_by_id(id: &str) -> HtmlTableRowElement {
    get_element_by_id::<HtmlTableRowElement>(id)
}

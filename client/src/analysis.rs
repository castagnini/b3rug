use wasm_bindgen::prelude::*;
use chrono;
use maud::{html, Markup, PreEscaped};

use crate::dbclient::CATEGORIES_MUTEX;
use crate::dbclient::CATEGORYMAP_MUTEX; 
use crate::dbclient::EXERCISETYPES_MUTEX;
use crate::dbclient::EXERCISES_MUTEX;
use crate::helpers::get_html_element_by_id;

use chrono::{NaiveDate, Datelike};
use std::collections::HashMap;

fn days_in_month (y:i32, m:u32) -> i64 {
    if m == 12 {
        NaiveDate::from_ymd_opt(y + 1, 1, 1).unwrap()
    } else {
        NaiveDate::from_ymd_opt(y, m + 1, 1).unwrap()
    }.signed_duration_since(NaiveDate::from_ymd_opt(y, m, 1).unwrap())
    .num_days()
}

fn categories_vs_calendar() -> String {
    let categories = CATEGORIES_MUTEX.lock().unwrap();
    let cm = CATEGORYMAP_MUTEX.lock().unwrap();
    let exercisetypes = EXERCISETYPES_MUTEX.lock().unwrap();
    let mut exercises = EXERCISES_MUTEX.lock().unwrap();
    if exercisetypes.len() == 0 {
        return format!{"Add exercise types first"}
    }
    if exercises.len() == 0 {
        return format!{"Add exercises first"}
    }
    if cm.len() == 0 {
        return format!{"Add cm first"}
    }
    if categories.len() == 0 {
        return format!{"Add categories first"}
    }
    
    let cat_size = categories.len();
    let mut map_categories_by_id = HashMap::new();
    for i in 0..cat_size {
        map_categories_by_id.insert(categories[i].id, i);
    }
    let mut inner: String = "".to_string();
    
    let mut map_cm_by_id = HashMap::new();
    for x in (&*cm).iter() {
        map_cm_by_id.entry(&x.exercise_id).or_insert(vec![]);
        map_cm_by_id.get_mut(&x.exercise_id).unwrap().push(x.category_id);
    }

    let mut calendar = HashMap::new();
    let mut days_per_month = HashMap::new();
    let mut key_vec = vec![];

    exercises.sort_by(|a, b| b.t.cmp(&a.t));
    
    for ex in (&*exercises).iter() {
        let key = ex.t.format("%B %Y").to_string();
        if !calendar.contains_key(&key) { key_vec.push(key.clone()); }
        calendar.entry(key.clone()).or_insert(vec![0; 31*cat_size]);
        days_per_month.entry(key.clone()).or_insert(days_in_month(ex.t.year(), ex.t.month())) ;      
        for c in map_cm_by_id[&ex.exercisetypeid].iter() {
            let cat_iter = map_categories_by_id[c];
            let x:usize =  cat_iter*31 + <u32 as TryInto<usize>>::try_into(ex.t.day0()).unwrap();
            calendar.get_mut(&key).unwrap()[x] = 1;
        }
    }
    
    let colors: [String;7] = ["table-primary".to_string(), 
                              "table-secondary".to_string(), 
                              "table-success".to_string(), 
                              "table-danger".to_string(), 
                              "table-warning".to_string(), 
                              "table-info".to_string(), 
                              "table-dark".to_string()];

    for m in (&*key_vec).iter() {
        let t = &calendar[m];
        let days = days_per_month[m].clone();
        let month_header:Markup = html! {
            h2 { (m) }
        };
        inner += month_header.into_string().as_str();
        let cat_table:Markup = html!{
            table class="table table-bordered" style="width: 100%" {
                tr {
                    th { "Categories" }
                    
                    @for d in 1..(days+1) {
                        th { (format!("{:0>2}", d)) }
                    }
                }
                @for c in 0..cat_size {
                    @let color = &colors[c%7];
                    tr {
                        td { (categories[c].name) }
                        @for d in 0..days {
                            @if t[c*31 +  <i64 as TryInto<usize>>::try_into(d).unwrap()] > 0 {
                                td class=(color) { "  " }
                            } @else{
                                td class="table-default" { "  " }
                            }
                        }
                    }

                }

            } 
        };
        inner += cat_table.into_string().as_str(); 

    };

    inner 
}

fn markup_accordion_analysis() -> String {
    let mut inner: String = "".to_string();
    let category_vs_calendar_table = categories_vs_calendar();
    let accordion:Markup = html!{
        div class="accordion" id="accordionPanelsStayOpenExample" {
            div class="accordion-item" {
                h2 class="accordion-header" id="panelsStayOpen-headingOne" {
                    button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne" {
                        "Categories vs. Calendar"
                    }
                }
                div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse " aria-labelledby="panelsStayOpen-headingOne" {
                    div class="accordion-body" {
                            (PreEscaped(category_vs_calendar_table))
                        table {
                            tr {
                                th { "3232" }
                            }
                            tr {
                                td { "eee" }
                            }
                        }
                    }
                }
            }
            div class="accordion-item" {
                h2 class="accordion-header" id="panelsStayOpen-headingTwo" {
                    button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo" {
                        "Filteractivities2"
                    }
                }
                div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse " aria-labelledby="panelsStayOpen-headingTwo" {
                    div class="accordion-body" {
                        "nothing"
                    }
                }
            }
        }
    };
    inner += accordion.into_string().as_str();
    
    inner
}


#[wasm_bindgen]
pub fn analysis() {
    let inner :String = markup_accordion_analysis().to_string();

    get_html_element_by_id("maindiv").set_inner_html(inner.as_str());
    
}


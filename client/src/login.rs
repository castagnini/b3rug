
use wasm_bindgen::prelude::*;
use maud::{html, Markup};
use wasm_cookies::CookieOptions;
use crypto::sha3::Sha3;
use crypto::digest::Digest;    
use crate::dashboard::dashboard;
use crate::model::User;
use crate::helpers::{get_span_element_by_id, get_input_element_by_id, get_html_element_by_id};

fn logout_form() -> String {
   let m = html!{
        form {
                button type="submit" onclick="logout()" class="btn btn-primary" { "Logout"} 
        }
    };

    m.into_string()
}

fn login_form() -> String {

    let m = html!{
        form {
            div { "There is only one user currently. Without credential you can still see stuff but not edit it." }
            div class="mb-3" {
                label for="username"  class="form-label" {"Name " }
                input class="form-control" id="username" aria-describedby="emailHelp" placeholder="Username" { } 
            }
            div class="mb-3" {
                label for="email"  class="form-label" {"Email " }
                input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" { }
                div id="emailHelp" class="form-text" {"Incoming spam in 3... 2... 1..." }
            }
            div class="mb-3" {
                label for="password" class="form-label" { "Password " }
                input type="password" class="form-control" id="password" placeholder="Password" {}
            }
            button type="submit" onclick="login()" class="btn btn-primary" { "Submit"} 
        }
    };
    m.into_string()
}

pub fn get_cookies() -> User {
    let mut u:User = User {
            id: 0,
            username:"".to_string(),
            email: "".to_string(),
            password: "".to_string()};

    let username:String;
    let mut cookie = wasm_cookies::get_raw("username");
    match cookie {
        Some(v) => username = v,
            None => return u,
    };

    let email:String;
    cookie = wasm_cookies::get_raw("email");
    match cookie {
        Some(v) => email = v,
            None => return u,
    };

    let password:String;
    let cookie = wasm_cookies::get_raw("password");
    match cookie {
        Some(v) => password = v,
            None => return u,
    };
    u = User {
        id: 0,
        username:username,
            email:email,
            password:password
        };

    u
}

pub  async fn check_login() {
    let c: String;
    let u: String;
    if !check_cookies() {
        c = "fs-5 bi-box-arrow-in-right".to_string();
        u = "Login".to_string();
    } else {
        c = "fs-5 bi-person-fill".to_string();
        u = "Logout".to_string();
    };
      
    get_html_element_by_id("user_img").set_class_name(&c);
    get_span_element_by_id("user_label").set_inner_html(&u);
    
}

#[wasm_bindgen]
pub async fn logout()  {
    wasm_cookies::delete_raw("username");
    wasm_cookies::delete_raw("password");
    wasm_cookies::delete_raw("email");

    dashboard().await;
}

#[wasm_bindgen]
pub async fn login() {
    let name = get_input_element_by_id("username").value();
    let pass = get_input_element_by_id("password").value();
    let email = get_input_element_by_id("email").value();

    if name.len()  == 0 || pass.len() == 0 || email.len() == 0 {
        return ;
    }

    let mut hasher = Sha3::sha3_256();
        hasher.input_str(&pass);
    let hashed =  hasher.result_str();

    wasm_cookies::set_raw("username", &name, &CookieOptions::default());
    wasm_cookies::set_raw("password", &hashed, &CookieOptions::default());
    wasm_cookies::set_raw("email", &email, &CookieOptions::default());

    dashboard().await;
}

pub fn check_cookies () -> bool{
    let u = get_cookies();
    if (u.email.len() > 0 ) &&  (u.password.len() > 0) && ( u.username.len() > 0 ) { 
        return true;
    } else {
        return false;
    }
}

#[wasm_bindgen]
pub async fn manage_user() {
    let page:String;
    if !check_cookies() {
        page = login_form();
    } else {
        page = logout_form();
    };


    get_html_element_by_id("maindiv").set_inner_html(page.as_str());
}


#[wasm_bindgen]
pub async fn rondella() {

    let m:Markup = html!{
        div class="spinner-border text-primary" role="status" {
            span class="sr-only" { "Loading..."}
        }
        div class="spinner-border text-success" role="status" {
            span class="sr-only" { "Loading..."} 
        }
        div class="spinner-border text-danger" role="status" {
            span class="sr-only" { "Loading..." }
        }
        div class="spinner-border text-warning" role="status" {
            span class="sr-only" {"Loading..."}
        }
    };

    get_html_element_by_id("maindiv").set_inner_html(m.into_string().as_str());
}


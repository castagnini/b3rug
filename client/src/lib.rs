
#[path = "../../model/model.rs"] pub mod model;

mod dbclient;
mod login;
mod dashboard;
mod categories;
mod exercises;
mod new_activity;
mod settings;
mod analysis;
mod helpers;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

 const SITEURL :&str = "https://castagnini.org";
 const SITEPORT:&str = "9970";
 //const SITEURL: &str = "http://127.0.0.1";
 //const SITEPORT:&str = "8000";

//const SITEURL: &str = "http://100.65.0.101";
//const SITEPORT:&str = "8000";


use chrono;
use wasm_bindgen::prelude::*;
use maud::{html, Markup};
use crate::model::{NewExercise, ExerciseType,Category, CategoryMap};
use crate::dbclient::{get_exercisetypes, add_exercises};
use crate::dbclient::{get_categories, get_categorymap};
use std::sync::Mutex;
use crate::dashboard::dashboard;
use crate::helpers::{get_table_row_element_by_id, get_select_element_by_id, get_input_element_by_id, get_html_element_by_id};

static CATEGORIES_MUTEX:Mutex<Vec<Category>> = Mutex::new(vec![]);
static CM_MUTEX: Mutex<Vec<CategoryMap>> =  Mutex::new(vec![]);
static EXERCISES_MUTEX: Mutex<Vec<ExerciseType>> = Mutex::new(vec![]);

static SELECTED_CATEGORY_IDX:Mutex<i32> = Mutex::new(-1); 
static SELECTED_EXERCISE_IDX:Mutex<i32> = Mutex::new(-1); 


fn rewrite_exercises_dropdown () -> Markup{
    let cm = CM_MUTEX.lock().unwrap();
    let exercises = EXERCISES_MUTEX.lock().unwrap();
    let mut filtered_exercises:Vec<ExerciseType>;
    let sel_cat:i32 = *SELECTED_CATEGORY_IDX.lock().unwrap();
    let mut sel_ex = SELECTED_EXERCISE_IDX.lock().unwrap();

    let mut previous_sel_ex_found:bool = false;

    if sel_cat == -1 {
        filtered_exercises = exercises.clone();
        previous_sel_ex_found = true;
    } else {
        filtered_exercises = vec![];
        for e in &*exercises {
            for m in &*cm {
                if m.exercise_id == e.id && 
                    m.category_id == sel_cat {
                        filtered_exercises.push(e.clone());
                        if  e.id  == *sel_ex {
                            previous_sel_ex_found = true;
                        }
                        break;
                    }
            }
        }
    }

    if previous_sel_ex_found == false {
        *sel_ex = -1;
    }

    let sel_ex = *sel_ex;


    html!{
        @if sel_ex == -1 { 
            option value="-1"  selected  { "  All"  }
        } @else {
            option value="-1" { "  All"  }
        }
        @for e in filtered_exercises {
            @let value_str = format!("{}",e.id);
            @if sel_ex == e.id { 
                option value=(value_str) selected   { "  " (e.name) } 
            } @else {
                option value=(value_str)   { "  " (e.name) } 
            }
        }
    } 

}

#[wasm_bindgen]
pub fn change_category_event() {
    let name = get_select_element_by_id("categories-dropdown").value();

    let value: i32 = name.parse().unwrap();
    let mut sel_cat = SELECTED_CATEGORY_IDX.lock().unwrap();

    *sel_cat = value;
    drop(sel_cat);

    let inner = rewrite_exercises_dropdown().into_string();
    
    get_html_element_by_id("exercises-dropdown").set_inner_html(inner.as_str());
    
}

#[wasm_bindgen]
pub fn change_exercise_event() {
    let name = get_select_element_by_id("exercises-dropdown").value();

    let value: i32 = name.parse().unwrap();
    let mut sel_ex = SELECTED_EXERCISE_IDX.lock().unwrap();

    *sel_ex = value;

    if value == -1 {
        get_table_row_element_by_id("tr_kg").set_hidden(true);
        get_table_row_element_by_id("tr_sets").set_hidden(true);
        get_table_row_element_by_id("tr_reps").set_hidden(true);
        get_table_row_element_by_id("tr_duration").set_hidden(true);
        get_table_row_element_by_id("tr_starttime").set_hidden(true);
        get_table_row_element_by_id("tr_notes").set_hidden(true);
        get_table_row_element_by_id("tr_button").set_hidden(true);
    } else {
        let exercises = EXERCISES_MUTEX.lock().unwrap();
        let index = exercises.iter().position(|r| r.id == value).unwrap();
        let ex = &exercises[index];
        
        get_table_row_element_by_id("tr_kg").set_hidden(!ex.kg);
        get_table_row_element_by_id("tr_sets").set_hidden(!ex.sets);
        get_table_row_element_by_id("tr_reps").set_hidden(!ex.reps);
        get_table_row_element_by_id("tr_duration").set_hidden(!ex.duration);
        get_table_row_element_by_id("tr_starttime").set_hidden(false);
        get_table_row_element_by_id("tr_notes").set_hidden(false);
        get_table_row_element_by_id("tr_button").set_hidden(false);
    }

    
}

fn markup_new_activity() -> Markup {
    let categories = CATEGORIES_MUTEX.lock().unwrap();
    let exercises = EXERCISES_MUTEX.lock().unwrap();
    if exercises.len() == 0 {
        return html!{"Add exercise types first"}
    }
    let sel_cat:i32 = * SELECTED_CATEGORY_IDX.lock().unwrap();
    let sel_ex:i32 = * SELECTED_EXERCISE_IDX.lock().unwrap();
    let mut adesso = format!("{:?}", chrono::offset::Local::now());
    let pos = adesso.rfind(':').unwrap();
    adesso.truncate(pos);
    let pos = adesso.rfind(':').unwrap();
    adesso.truncate(pos);
    html!{
        h3 { "Add Activity:" }
         p class="lead" {
            table class="table table-fit table-borderless w-auto" { 
                tr {
                    td { "category:" }
                    td {}
                }
                tr {
                    td colspan="2" {
                        select class="form-select" id="categories-dropdown" onChange="change_category_event()" {
                             @if sel_cat == -1 { 
                                 option value="-1"  selected  { "  All"  }
                              } @else {
                                 option value="-1" { "  All"  }
                              }
                            @for c in &*categories {
                                @let value_str = format!("{}",c.id);
                             @if sel_cat == c.id { 
                                option value=(value_str) selected   { "  " (c.name) } 
                              } @else {
                                option value=(value_str) { "  " (c.name) } 
                              }
                            }
                        }
                    }
                    td {}
                }
                tr {
                    td { "Exercise:" }
                    td {}
                }
                tr {
                    td colspan="2" {
                        select class="form-select" id="exercises-dropdown" onChange="change_exercise_event()"  {
                            @if sel_ex == -1 { 
                                option value="-1"  selected  { "  All"  }
                            } @else {
                                option value="-1" { "  All"  }
                            }
                            @for e in &*exercises {
                                @let value_str = format!("{}",e.id);
                                @if sel_ex == e.id { 
                                    option value=(value_str) selected   { "  " (e.name) } 
                                } @else {
                                    option value=(value_str)   { "  " (e.name) } 
                                }
                            }
                        }
                    }
                    td {}
                }
                tr id="tr_starttime" hidden="true" {
                    td { "Start time:" }
                    td {
                        input type="datetime-local" class="form-control" value=(adesso) defaultValue=(adesso) id="starttime_input"  { }
                    }
                }
                tr id="tr_kg" hidden="true" {
                    td { "kg:" }
                    td {
                        input type="number" class="form-control" id="kg_input" defaultValue="0.0" value="0.0" { }
                    }
                }
                tr id="tr_sets" hidden="true" {
                    td { "sets:" }
                    td {
                        input type="number" class="form-control" id="sets_input" defaultValue="1" value="1"  { }
                    }
                }
                tr id="tr_reps" hidden="true" {
                    td { "reps:" }
                    td {
                        input type="number" class="form-control" id="reps_input" defaultValue="1" value="1"  { }
                    }
                }
                tr id="tr_duration"  hidden="true" {
                    td { "duration:" }
                    td {
                        form {
                            input id="hours_input" name="h" type="number" min="0" defaultValue="0" value="0" { }
                            label for="h" {"h"}
                            input id="minutes_input" name="m" type="number" min="0" max="59" defaultValue="0" value="0" { }
                            label for="m" {"m"}
                            input id="seconds_input" name="s" type="number" min="0" max="59" defaultValue="0" value="0" { }
                            label for="s" {"s"}
                        } 
                    }
                }
                tr id="tr_notes" hidden="true" {
                    td { "Notes:" }
                    td {
                        input type="email" class="form-control" id="notes_input" { }
                    }
                }
                tr id="tr_button"  hidden="true" {
                    td {}
                    td {
                        button class="btn btn-primary" onClick="add_activity()" {
                            "Add " i class="fs-5 bi-plus" { } 
                        }
                    }
                }
            }
         }
         hr { }
    }
}

#[wasm_bindgen]
pub async fn add_activity() {
    
    let sel_ex = *SELECTED_EXERCISE_IDX.lock().unwrap();
        let exercises = EXERCISES_MUTEX.lock().unwrap();
        let index = exercises.iter().position(|r| r.id == sel_ex).unwrap();
        let ex = &exercises[index];

    let date_s = get_input_element_by_id("starttime_input").value();
    let kg_s = get_input_element_by_id("kg_input").value();
    let sets_s = get_input_element_by_id("sets_input").value();
    let reps_s = get_input_element_by_id("reps_input").value();
    let hours_s = get_input_element_by_id("hours_input").value();
    let minutes_s = get_input_element_by_id("minutes_input").value();
    let seconds_s = get_input_element_by_id("seconds_input").value();
    let notes_s = get_input_element_by_id("notes_input").value();

    let starttime = chrono::NaiveDateTime::parse_from_str(&date_s, "%Y-%m-%dT%H:%M").unwrap();

    let _h = hours_s.parse::<i32>().unwrap_or_else(|_| {0});
    let _s = seconds_s.parse::<i32>().unwrap_or_else(|_| {0});
    let _m = minutes_s.parse::<i32>().unwrap_or_else(|_| {0});
    
    let k = if ex.kg {kg_s.parse::<f32>().unwrap_or_else(|_| {0.0})} else {0.0};
    let s = if ex.sets {sets_s.parse::<i32>().unwrap_or_else(|_| {0})} else {0};
    let r = if ex.reps {reps_s.parse::<i32>().unwrap_or_else(|_| {0})} else {0};
    let d = if ex.duration {_h*3600 +_m*60 + _s} else {0};

    let new_ex = NewExercise { 
            exercisetypeid: sel_ex,
            t: starttime,
            notes: notes_s.as_str(),
            kg: k,
            sets: s,
            reps: r,
            duration: d
        };

    let _ = add_exercises(&new_ex).await;
    // refresh page
    let _ = dashboard().await; 
}

#[wasm_bindgen]
pub async fn new_activity() {

    let mut inner: String = "".to_string();

    let mut categories = CATEGORIES_MUTEX.lock().unwrap();
    *categories = get_categories().await;
    drop(categories);

    let mut exercises = EXERCISES_MUTEX.lock().unwrap();
    *exercises = get_exercisetypes().await;
    drop(exercises);

    let mut cm = CM_MUTEX.lock().unwrap();
    *cm = get_categorymap().await;
    drop(cm);

    let c: String =  markup_new_activity().into_string() ;
    inner += c.as_str(); 
    get_html_element_by_id("maindiv").set_inner_html(inner.as_str());
    change_category_event();
    change_exercise_event();
    

}


use wasm_bindgen::prelude::*;
use maud::{html, Markup};
use crate::model::{Category,NewCategory};
use crate::dbclient::{get_categories, get_categories_by_id, add_category, edit_category, delete_category, delete_categorymap_by_categoryid};
use crate::helpers::{get_input_element_by_id, get_html_element_by_id};

fn markup_add_category() -> Markup {
    html!{
        h3 { "Add Category:" }
         p class="lead" {
            table class="table table-fit table-borderless w-auto" { 
                tr {
                    td { "name:" }
                    td {}
                }
                tr {
                    td {
                        input type="email" class="form-control" id="new_category_name" { }
                    }
                    td {
                        button class="btn btn-primary" onClick="new_category()" {
                           i class="fs-5 bi-plus" { } 
                        }
                    }
                }
            }
        }
        hr { }
    }
}

fn markup_edit_category(c:&Category) -> Markup {
    html!{
        h3 { "Edit Category (id:"  (c.id)  "):" }
         p class="lead" {
            table class="table table-fit table-borderless w-auto" { 
                tr {
                    td { "old name:" }
                    td { (c.name) }
                }
                tr {
                    td { "new name:" }
                    td {}
                }
                tr {
                    td {
                        input type="email" class="form-control" id="new_category_name" { }
                    }
                    td {
                        button class="btn btn-primary" 
                        data-id=(c.id) 
                        onClick="modify_category(this.getAttribute(\"data-id\"))" {
                           i class="fs-5 bi-pencil" { } 
                        }
                    }
                }
            }
        }
        hr { }
    }
}

fn markup_categories_table(v :& Vec<Category>) -> Markup {
    html!{ 
        h3 { "Category list:" }
        table class="table table-striped table-bordered table-fit w-auto" { 
            tr {
                th { "name" }
                th {}
                th {}
            }
            @for c in v {
                // tr { td { a href=(l) { img width="200" src=(s) { } }} }
                tr {
                    td { 
                        (c.name)
                    } 
                    td {
                        button class="btn btn-secondary" 
                        data-id=(c.id.to_string()) 
                        onClick="edit_category_page(this.getAttribute(\"data-id\"))" {
                           i class="fs-5 bi-pencil" { } 
                        }
                    }
                    td {
                        button class="btn btn-danger" 
                        data-id=(c.id) 
                        onClick="remove_category(this.getAttribute(\"data-id\"))" {
                           i class="fs-5 bi-trash" { } 
                        }
                    }
                }
            }
        }
    }
}

#[wasm_bindgen]
pub async fn modify_category(id_s: String) {
    let id = id_s.parse::<i32>().unwrap();
    let name = get_input_element_by_id("new_category_name").value();

    if name.len()  == 0 {
        return ;
    }

    let c = NewCategory {name: name.as_str()};
    let _ = edit_category(&c, id).await;

    // refresh page
    let _ = categories().await; 
}

#[wasm_bindgen]
pub async fn new_category() {
    let name = get_input_element_by_id("new_category_name").value();

    if name.len()  == 0 {
        return ;
    }

    let c = NewCategory {name: name.as_str()};
    let _ = add_category(&c).await;

    // refresh page
    let _ = categories().await; 
}

#[wasm_bindgen]
pub async fn remove_category (id: String) {
    let id_int = id.parse::<i32>().unwrap();

    let _ = delete_category(id_int).await;
    let _ = delete_categorymap_by_categoryid(id_int).await;

    // refresh page
    let _ = categories().await; 
}


#[wasm_bindgen]
pub async fn categories() {

    let mut inner: String = "".to_string();

    let mut c: String =  markup_add_category().into_string() ;
    inner += c.as_str(); 
    let categories: Vec<Category> = get_categories().await;

    c =  markup_categories_table(&categories).into_string() ;
    inner += c.as_str(); 
    get_html_element_by_id("maindiv").set_inner_html(inner.as_str());
    
}

#[wasm_bindgen]
pub async fn edit_category_page(id_s: String) {
    let id = id_s.parse::<i32>().unwrap();

    let mut inner: String = "".to_string();

    let categories: Vec<Category> = get_categories_by_id(id).await;
    if categories.len() == 0 {
        return;
    }

    let c: String =  markup_edit_category(&categories[0]).into_string() ;
    inner += c.as_str(); 

    get_html_element_by_id("maindiv").set_inner_html(inner.as_str());
    
}




use wasm_bindgen::prelude::*;
use maud::{html, Markup};
use crate::model::{JsonInt32, ExerciseType,NewExerciseType,Category, CategoryMap};
use crate::dbclient::{get_exercisetypes, add_exercisetype, edit_exercisetype, delete_exercisetype, update_categorymap_ex_id};
use crate::dbclient::{get_categories, get_categorymap};

use crate::dbclient::delete_categorymap_by_exercisetypeid;
use crate::dbclient::delete_exercise_by_exercisetypeid;
use crate::helpers::{get_input_element_by_id, get_html_element_by_id};

fn markup_add_exercise(v:&Vec<Category>) -> Markup {
    html!{
        h3 { "Add Exercise:" }
         p class="lead" {
            table class="table table-fit table-borderless w-auto" { 
                tr {
                    td { "name:" }
                    td {}
                }
                tr {
                    td colspan="2" {
                        input type="email" class="form-control" id="new_exercise_name" { }
                    }
                    td {}
                }
                tr {
                    td { "description:" }
                    td {}
                }
                tr {
                    td colspan="2" {
                        input type="email" class="form-control" id="new_exercise_desc" { }
                    }
                    td {}
                }
                tr {
                    td { "has kg:" }
                    td {
                        input type="checkbox" class="form-check-input" value="" id="kg_cb"  { }
                    }
                }
                tr {
                    td { "has sets:" }
                    td {
                        input type="checkbox" class="form-check-input" value="" id="sets_cb"  { }
                    }
                }
                tr {
                    td { "has reps:" }
                    td {
                        input type="checkbox" class="form-check-input" value="" id="reps_cb"  { } 
                    }
                }
                tr {
                    td { "has duration:" }
                    td {
                        input type="checkbox" class="form-check-input" value="" id="duration_cb"  { }
                    }
                }
                tr {
                    td colspan="2" {
                        fieldset {
                            legend  class="form-legend" { "categories" } 
                            @for c in v {
                                @let id_str = format!("cat_id_{}",c.id);
                                div { 
                                    input type="checkbox" class="form-check-input" value="" 
                                        id=(id_str) data-id=(c.id) { "  " (c.name) } 
                                }
                            }

                        }
                    }
                    td {}
                }


                tr {
                    td {}
                    td {
                        button class="btn btn-primary" onClick="new_exercise()" {
                           "Add " i class="fs-5 bi-plus" { } 
                        }
                    }
                }
            }
        }
        hr { }
    }
}

/*
fn markup_edit_exercise(t:&ExerciseType, categories:&Vec<Category>, maps:&Vec<CategoryMap>) -> Markup {
    html!{
        h3 { "Edit Exercise (id:"  (t.id)  "):" }
         p class="lead" {
            table class="table table-fit table-borderless w-auto" { 
                tr {
                    td { "old name:" }
                    td { (c.name) }
                }
                tr {
                    td { "new name:" }
                    td {}
                }
                tr {
                    td {
                        input type="email" class="form-control" id="new_exercise_name" { }
                    }
                    td {
                        button class="btn btn-primary" 
                        data-id=(c.id) 
                        onClick="modify_exercise(this.getAttribute(\"data-id\"))" {
                           i class="fs-5 bi-pencil" { } 
                        }
                    }
                }
            }
        }
        hr { }
}
}
*/

fn markup_exercises_table(types :& Vec<ExerciseType>, categories: &Vec<Category>, cm: &Vec<CategoryMap> ) -> Markup {
    html!{ 
        h3 { "List of exercises:" }
        table class="table table-striped table-bordered table-fit w-auto" { 
            tr {
                th { "name" }
                th { "description" }
                th { "kg" }
                th { "sets" }
                th { "reps" }
                th { "duration" }
                th { "categories" }
                th {}
                th {}
            }
            @for t in types {
                // tr { td { a href=(l) { img width="200" src=(s) { } }} }
                tr {
                    td { 
                        (t.name)
                    } 
                    td { 
                        (t.description)
                    } 
                    td { 
                        @if t.kg { "kg" }
                    } 
                    td { 
                        @if t.sets { "sets" }
                    } 
                    td { 
                        @if t.reps { "reps" }
                    } 
                    td { 
                        @if t.duration { "duration" }
                    }
                    td { 
                        @for m in cm {
                            @if m.exercise_id == t.id {
                                @for c in categories {
                                    @if c.id == m.category_id {
                                        (c.name) " " 
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    td {
                        button class="btn btn-secondary" 
                        data-id=(t.id.to_string()) 
                        onClick="edit_exercise_page(this.getAttribute(\"data-id\"))" {
                           i class="fs-5 bi-pencil" { } 
                        }
                    }
                    td {
                        button class="btn btn-danger" 
                        data-id=(t.id) 
                        onClick="remove_exercise(this.getAttribute(\"data-id\"))" {
                            i class="fs-5 bi-trash" { } 
                        }
                    }
                }
            }
        }
    }
}

#[wasm_bindgen]
pub async fn modify_exercise(id_s: String) {
    let id = id_s.parse::<i32>().unwrap();
    let name = get_input_element_by_id("new_exercise_name").value();

    if name.len()  == 0 {
        return ;
    }
    
    let desc = get_input_element_by_id("new_exercise_desc").value();
    let kg_cb = get_input_element_by_id("kg_cb").checked();
    let sets_cb = get_input_element_by_id("sets_cb").checked();
    let reps_cb = get_input_element_by_id("reps_cb").checked();
    let duration_cb = get_input_element_by_id("duration_cb").checked();

    let c = NewExerciseType { name: name.as_str(),
            description: desc.as_str(),
            kg: kg_cb,
            sets: sets_cb,
            reps: reps_cb,
            duration: duration_cb
        };

    let _ = edit_exercisetype(&c, id).await;

    // refresh page
    let _ = exercises().await; 
}

#[wasm_bindgen]
pub async fn new_exercise() {
    let name = get_input_element_by_id("new_exercise_name").value();

    if name.len()  == 0 {
        return ;
    }

    let desc = get_input_element_by_id("new_exercise_desc").value();
    let kg_cb = get_input_element_by_id("kg_cb").checked();
    let sets_cb = get_input_element_by_id("sets_cb").checked();
    let reps_cb = get_input_element_by_id("reps_cb").checked();
    let duration_cb = get_input_element_by_id("duration_cb").checked();

    let mut cats_for_exercise:Vec<i32> = vec![];
    let categories: Vec<Category> = get_categories().await;

    for c in categories {
        let id_str = format!("cat_id_{}",c.id);
        let cat_cb = get_input_element_by_id(&id_str).checked();
        if cat_cb {
            cats_for_exercise.push(c.id);
        }
    }

    let c = NewExerciseType { name: name.as_str(),
            description: desc.as_str(),
            kg: kg_cb,
            sets: sets_cb,
            reps: reps_cb,
            duration: duration_cb
        };
    let id_vec: Vec<i32> = add_exercisetype(&c).await;

    if id_vec.len() == 0 { return; }
    let ex_id = id_vec[0];
    let p = JsonInt32 {data: cats_for_exercise};
    let _ = update_categorymap_ex_id(p, ex_id).await;

    // refresh page
    let _ = exercises().await; 
}

#[wasm_bindgen]
pub async fn remove_exercise (id: String) {
    let id_int = id.parse::<i32>().unwrap();

    let _ = delete_exercise_by_exercisetypeid(id_int).await;
    let _ = delete_categorymap_by_exercisetypeid(id_int).await;
    let _ = delete_exercisetype(id_int).await;

    // refresh page
    exercises().await; 
}


#[wasm_bindgen]
pub async fn exercises() {

    let mut inner: String = "".to_string();

    let categories: Vec<Category> = get_categories().await;
    let mut c: String =  markup_add_exercise(&categories).into_string() ;
    inner += c.as_str(); 
    let cm: Vec<CategoryMap> = get_categorymap().await;
    let exercises: Vec<ExerciseType> = get_exercisetypes().await;

    c =  markup_exercises_table(&exercises, &categories, &cm).into_string() ;
    inner += c.as_str(); 
    
    get_html_element_by_id("maindiv").set_inner_html(inner.as_str());
    
}



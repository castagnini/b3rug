
use wasm_bindgen::prelude::*;

use crate::SITEURL;
use crate::SITEPORT;
use crate::model::{JsonInt32, JsonCategory, JsonCategoryMap, JsonExercise, JsonExerciseType};
use crate::model::{ /*User,*/ NewCategory, Category,  NewCategoryMap, CategoryMap, NewExerciseType, ExerciseType, NewExercise, Exercise};
use crate::login::get_cookies;
use std::sync::Mutex;

pub static CATEGORIES_MUTEX:Mutex<Vec<Category>> = Mutex::new(vec![]);
pub static CATEGORYMAP_MUTEX: Mutex<Vec<CategoryMap>> =  Mutex::new(vec![]);
pub static EXERCISETYPES_MUTEX: Mutex<Vec<ExerciseType>> = Mutex::new(vec![]);
pub static EXERCISES_MUTEX: Mutex<Vec<Exercise>> = Mutex::new(vec![]);

pub async fn refresh_categories() {
    let mut categories = CATEGORIES_MUTEX.lock().unwrap();
    *categories = get_categories().await;
}

pub async fn refresh_exercises() {
    let mut exercises = EXERCISES_MUTEX.lock().unwrap();
    *exercises = get_exercises().await;
}

pub async fn refresh_exercisetypes() {
    let mut exercisetypes = EXERCISETYPES_MUTEX.lock().unwrap();
    *exercisetypes = get_exercisetypes().await;
}

pub async fn refresh_categorymap() {
    let mut cm = CATEGORYMAP_MUTEX.lock().unwrap();
    *cm = get_categorymap().await;
}

pub async fn refresh_all_db_mutexes () {
     refresh_categories().await;
     refresh_exercisetypes().await;
     refresh_exercises().await;
     refresh_categorymap().await;
}

macro_rules! create_get_entries {
    ($func_name:ident, $hook_name:literal,$vectype:ident , $entrytype:ty ) => {

pub async fn $func_name() -> Vec<$entrytype> {
    let url = format!("{}:{}/{}", SITEURL, SITEPORT, $hook_name);
    let res = reqwest::Client::new()
        .get(url)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .send().await.unwrap();

    let text = res.text().await.unwrap();
    let json_res: $vectype = serde_json::from_str(&text).unwrap(); 

    json_res.data

    }
};
}
create_get_entries!(get_categories, "category", JsonCategory, Category);
create_get_entries!(get_categorymap, "categorymap", JsonCategoryMap, CategoryMap);
create_get_entries!(get_exercisetypes, "exercisetype",JsonExerciseType, ExerciseType );
create_get_entries!(get_exercises, "exercise",JsonExercise, Exercise );

macro_rules! create_get_entries_by_id {
    ($func_name:ident, $hook_name:literal,$vectype:ident , $entrytype:ty ) => {

#[allow(dead_code)]
pub async fn $func_name(id:i32) -> Vec<$entrytype> {
    let url = format!("{}:{}/{}/by_id/{}", SITEURL, SITEPORT, $hook_name,id);


    let res = reqwest::Client::new()
        .get(url)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .send().await.unwrap();

    let text = res.text().await.unwrap();
    let json_res: $vectype = serde_json::from_str(&text).unwrap(); 

    json_res.data

    }
};
}
create_get_entries_by_id!(get_categories_by_id, "category", JsonCategory, Category);
create_get_entries_by_id!(get_categorymap_by_id, "categorymap", JsonCategoryMap, CategoryMap);
create_get_entries_by_id!(get_exercisetypes_by_id, "exercisetype",JsonExerciseType, ExerciseType );
create_get_entries_by_id!(get_exercises_by_id, "exercise",JsonExercise, Exercise );


macro_rules! create_del_entries {
    ($func_name:ident, $hook_name:literal) => {

#[allow(dead_code)]
pub async fn $func_name(id: i32) -> Result<JsValue, JsValue> {
    let url = format!("{}:{}/{}/{}", SITEURL, SITEPORT, $hook_name, id);

    let u = get_cookies();

    reqwest::Client::new()
        .delete(url)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .header("username",u.username)
        .header("email",u.email)
        .header("password",u.password)
        .send().await?;

    Ok( JsValue::TRUE )
        }
};
}
create_del_entries!(delete_category, "category/del");
create_del_entries!(delete_categorymap, "categorymap/del");
create_del_entries!(delete_categorymap_by_categoryid, "categorymap/del_by_cat_id");
create_del_entries!(delete_categorymap_by_exercisetypeid, "categorymap/del_by_ex_id");
create_del_entries!(delete_exercisetype, "exercisetype/del");
create_del_entries!(delete_exercises, "exercise/del");
create_del_entries!(delete_exercise_by_exercisetypeid, "exercise/del_by_type_id");

macro_rules! create_add_entries {
    ($func_name:ident, $hook_name:literal, $entrytype:ty) => {

#[allow(dead_code)]
pub async fn $func_name(p : & $entrytype) -> Vec<i32> {
    let url = format!("{}:{}/{}/add/", SITEURL, SITEPORT, $hook_name);
    
    let u = get_cookies();

    let res = reqwest::Client::new()
        .post(url).json(&p)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .header("username",u.username)
        .header("email",u.email)
        .header("password",u.password)
        .send().await.unwrap();

    let text = res.text().await.unwrap();
    let json_res: JsonInt32 = serde_json::from_str(&text).unwrap(); 

    json_res.data
        }
};
}
create_add_entries!(add_category, "category", NewCategory<'_>);
create_add_entries!(add_categorymap, "categorymap", NewCategoryMap);
create_add_entries!(add_exercisetype, "exercisetype", NewExerciseType<'_>);
create_add_entries!(add_exercises, "exercise", NewExercise<'_>);

macro_rules! create_edit_entries {
    ($func_name:ident, $hook_name:literal, $entrytype:ty) => {

#[allow(dead_code)]
pub async fn $func_name(p : & $entrytype, id: i32) -> Result<JsValue, JsValue> {
    let url = format!("{}:{}/{}/edit/{}", SITEURL, SITEPORT, $hook_name, id);
    let u = get_cookies();
    reqwest::Client::new()
        .post(url).json(&p)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .header("username",u.username)
        .header("email",u.email)
        .header("password",u.password)
        .send().await?;

    Ok( JsValue::TRUE )
        }
};
}
create_edit_entries!(edit_category, "category", NewCategory<'_>);
create_edit_entries!(edit_categorymap, "categorymap", NewCategoryMap);
create_edit_entries!(edit_exercisetype, "exercisetype", NewExerciseType<'_>);
create_edit_entries!(edit_exercises, "exercise", NewExercise<'_>);


pub async fn update_categorymap_ex_id(p : JsonInt32, id: i32) -> Result<JsValue, JsValue> {
    let url = format!("{}:{}/categorymap/update_ex_id/{}", SITEURL, SITEPORT, id);
    let u = get_cookies();
    reqwest::Client::new()
        .post(url).json(&p)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .header("username",u.username)
        .header("email",u.email)
        .header("password",u.password)
        .send().await?;

    Ok( JsValue::TRUE )
}


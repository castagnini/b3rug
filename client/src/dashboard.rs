use wasm_bindgen::prelude::*;
use maud::{html, Markup};
use crate::dbclient::{delete_exercises};
use crate::dbclient::{refresh_all_db_mutexes};
use crate::login::check_login;
use crate::helpers::get_html_element_by_id;

use crate::dbclient::EXERCISETYPES_MUTEX;
use crate::dbclient::EXERCISES_MUTEX;

fn markup_list_activities() -> String {
    let exercisetypes = EXERCISETYPES_MUTEX.lock().unwrap();
    let exercises = EXERCISES_MUTEX.lock().unwrap();
    if exercisetypes.len() == 0 {
        return format!{"Add exercise types first"}
    }
    if exercises.len() == 0 {
        return format!{"Add exercises first"}
    }

    let mut inner: String = "".to_string();

    let accordion:Markup = html!{
        div class="accordion" id="accordionPanelsStayOpenExample" {
            div class="accordion-item" {
                h2 class="accordion-header" id="panelsStayOpen-headingOne" {
                    button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="false" aria-controls="panelsStayOpen-collapseOne" {
                        "Filteractivities"
                    }
                }
                div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse " aria-labelledby="panelsStayOpen-headingOne" {
                    div class="accordion-body" {
                        "There will be filters here"
                    }
                }
            }
        }
    };
    inner += accordion.into_string().as_str();

    for ex in (&*exercises).iter().rev() {

        let m:Markup = html!{
                        button class="btn btn-danger" 
                        data-id=(ex.id) 
                        onClick="remove_activity(this.getAttribute(\"data-id\"))" {
                            i class="fs-5 bi-trash" { } 
                        }
        };

        inner += "<hr />";
        let index:usize;
        match exercisetypes.iter().position(|r| r.id == ex.exercisetypeid) {
            Some(i) => index = i,
            None => {
                    inner += format!("<h3>lost entry with id:{} and unknown exercisetypeid {} </h3>", ex.id, ex.exercisetypeid).as_str();
                    inner += m.into_string().as_str(); 
                    continue;
                },
        }
        let etype = &exercisetypes[index];
        
    

        inner += format!("<h3>{}</h3>", etype.name).as_str(); 
        inner += format!("<p class=\"lead\">Time: {}</p>", ex.t).as_str();
        if etype.kg { 
            inner += format!("<p class=\"lead\">Kg: {}</p>", ex.kg).as_str(); 
        }
        if etype.sets { 
            inner += format!("<p class=\"lead\">sets: {}</p>", ex.sets).as_str(); 
        }
        if etype.reps { 
            inner += format!("<p class=\"lead\">reps: {}</p>", ex.reps).as_str(); 
        }
        if etype.duration { 
            inner += format!("<p class=\"lead\">duration: {} s</p>", ex.duration).as_str(); 
        }
        if ex.notes.trim().len() > 0 {
            inner += format!("<p class=\"lead\">notes: {} s</p>", ex.notes).as_str(); 
        }

               inner += m.into_string().as_str(); 
    }
    
    inner
}

#[wasm_bindgen]
pub async fn remove_activity(id: String) {
    let id_int = id.parse::<i32>().unwrap();

    let _ = delete_exercises(id_int).await;

    // refresh page
    let _ = dashboard().await; 

}

#[wasm_bindgen]
pub async fn dashboard() {

   // check_login();

    let mut inner: String = "".to_string();
    let _ = refresh_all_db_mutexes().await;

    let c: String =  markup_list_activities() ;
    inner += c.as_str(); 

    get_html_element_by_id("maindiv").set_inner_html(inner.as_str());
   
    let _ = check_login().await;
}

